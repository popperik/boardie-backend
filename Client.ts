import Player from "boardie/dist/Player";
import BoardieEvent from "./events/BoardieEvent";
import ErrorEvent from "./events/ErrorEvent";
import WebSocket from "ws";

export default class {
    ws: WebSocket;
    player: Player | undefined;

    constructor(ws: WebSocket) {
        this.ws = ws;
    }

    send(event: BoardieEvent): void {
        console.log(`Sending message: ${JSON.stringify(event)}`);
        this.ws.send(JSON.stringify(event));
    }

    sendError(message: string): void {
        this.send(new ErrorEvent(message));
    }
};
