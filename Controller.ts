import boardie from "boardie";
import Game from "boardie/dist/Game";
import express from "express";
import WebSocket from "ws";
import PlayerJoinedEvent from "./events/PlayerJoinedEvent";
import LogEvent from "./events/LogEvent";
import GameStartingEvent from "./events/GameStartingEvent";
import GameCreatedEvent from "./events/GameCreatedEvent";
import Decision from "boardie/dist/Decision";
import Player from "boardie/dist/Player";
import Client from "./Client";
import NewDecisionEvent from "./events/NewDecisionEvent";
import SetupCompletedEvent from "./events/SetupCompletedEvent";
import PlayerStateChangedEvent from "./events/PlayerStateChangedEvent";
import { DecisionHandler } from "boardie/dist/Common";

const games: Game[] = [];
const gamePlayers = new Map<string, Client[]>();
const gameDefinitions = boardie.gameDefinitions;
const clients: Client[] = [];

function findGame(gameId: string): Game | undefined {
    return games.find(game => game.data.id == gameId);
}

function gameDefinitionsHandler(req: express.Request, res: express.Response): void {
    res.json(gameDefinitions);
}

function gamesHandler(req: express.Request, res: express.Response): void {
    res.send(games);
}

function getGameHandler(req: express.Request, res: express.Response): void {
    const game = findGame(req.params.gameId);
    if (!game) {
        res.status(404).send("No game with that ID exists");
        return;
    }

    res.json(game);
}

function createNewGame(gameName: string, playerName: string, client: Client): void {
    const index = gameDefinitions.findIndex(gameDefinition => gameDefinition.name == gameName);
    if (index < 0) {
        client.sendError("No game definition with that name exists");
        return;
    }

    const game = new Game(gameDefinitions[index]);
    game.logHandler = getLogHandler(game);
    game.decisionHandler = getDecisionHandler(game);
    game.setupCompletedHandler = setupCompletedHandler;
    game.playerStateChangedHandler = playerStateChangedHandler;
    games.push(game);
    gamePlayers.set(game.data.id, []);
    clients.forEach(client => client.send(new GameCreatedEvent(game)));

    addUserToGame(game.data.id, playerName, false, client);
}

function startGameHandler(req: express.Request, res: express.Response): void {
    const game = findGame(req.params.gameId);
    if (!game) {
        res.status(404).send("No game with that ID exists");
        return;
    }

    gamePlayers.get(game.data.id)!.forEach(client => client.send(new GameStartingEvent()));
    game.play(); // TODO catch
    res.sendStatus(204);
}

function setupCompletedHandler(game: Game): void {
    gamePlayers.get(game.data.id)!.forEach(client => client.send(new SetupCompletedEvent(game.data.viewedAs(client.player!))));
}

function playerStateChangedHandler(game: Game, player: Player): void {
    gamePlayers.get(game.data.id)!.forEach(client => client.send(new PlayerStateChangedEvent(player.viewedAs(client.player!))));
}

function getDecisionHandler(game: Game): DecisionHandler {
    return (decision: Decision, target: Player) => {
        const client = gamePlayers.get(game.data.id)!.find(c => c.player!.id == target.id);
        if (!client) {
            throw new Error("Couldn't find decision target");
        }

        client.send(new NewDecisionEvent(decision));
        return new Promise((resolve, reject) => {
            const handler = (message: any) => {
                const data = JSON.parse(message.toString());
                if (data.event == "makeDecision" && data.decisionId == decision.id) {
                    const choice = decision.choices.find(c => c.id == data.choiceId);
                    if (!choice) {
                        client.sendError("Invalid choice");
                        reject("Invalid choice");
                    } else {
                        resolve(choice);
                    }

                    // Every listener can only listen once for now
                    client.ws.removeListener("message", handler);
                }
            };
            client.ws.on("message", handler); // TODO this needs to be cleaned up if game is interrupted
        });
    }
}

function getLogHandler(game: Game): (message: string) => void {
    return (message: string) => gamePlayers.get(game.data.id)!.forEach(client => client.send(new LogEvent(message)));
}

function addUserToGame(gameId: string, playerName: string, ai: boolean, client: Client): void {
    const game = findGame(gameId);
    if (!game) {
        client.sendError("No game with that ID exists");
        return;
    }

    try {
        const player = game.addPlayer(playerName, ai);
        if (!ai) {
            client.player = player;
            gamePlayers.get(gameId)!.push(client);
        }

        gamePlayers.get(gameId)!.forEach(client => client.send(new PlayerJoinedEvent(game)));
    } catch (error) {
        client.sendError(error.message);
        throw error;
    }
}

export function registerRoutes(app: express.Application, wss: WebSocket.Server): void {
    app.get("/gameDefinitions", gameDefinitionsHandler);
    app.get("/games", gamesHandler);
    app.get("/games/:gameId", getGameHandler);
    app.post("/games/:gameId/start", startGameHandler);

    wss.on("connection", (ws, req) => { // TODO authenticate
        console.log(`${req.socket.remoteAddress} connected`);
        const client = new Client(ws);
        clients.push(client);

        ws.on("message", (message) => {
            const data = JSON.parse(message.toString());
            console.log(`Incoming message: ${JSON.stringify(data)}`);
            switch (data.event) {
                case "join": addUserToGame(data.gameId, data.playerName, data.ai, client); break;
                case "makeDecision": break; // Handled elsewhere
                case "newGame": createNewGame(data.gameName, data.playerName, client); break;
                default: console.log(`Unrecognised event type: ${data.event}`);
            }
        });
    });
}