import BoardieEvent from "./BoardieEvent";
import Decision from "boardie/dist/Decision";

export default class NewDecisionEvent extends BoardieEvent {
    static readonly NAME = "newDecision";
    readonly decision: Decision;

    constructor(decision: Decision) {
        super(NewDecisionEvent.NAME);
        this.decision = decision;
    }
}