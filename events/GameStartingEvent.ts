import BoardieEvent from "./BoardieEvent";

export default class GameStartingEvent extends BoardieEvent {
    static readonly NAME = "gameStarting";

    constructor() {
        super(GameStartingEvent.NAME);
    }
}