import BoardieEvent from "./BoardieEvent";

export default class ErrorEvent extends BoardieEvent {
    static readonly NAME = "error";
    readonly message: string;

    constructor(message: string) {
        super(ErrorEvent.NAME);
        this.message = message;
    }
}