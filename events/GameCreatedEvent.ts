import BoardieEvent from "./BoardieEvent";
import Game from "boardie/dist/Game";

export default class GameCreatedEvent extends BoardieEvent {
    static readonly NAME = "gameCreated";
    readonly game: Game;

    constructor(game: Game) {
        super(GameCreatedEvent.NAME);
        this.game = game;
    }
}