import BoardieEvent from "./BoardieEvent";

export default class LogEvent extends BoardieEvent {
    static readonly NAME = "log";
    readonly message: string;

    constructor(message: string) {
        super(LogEvent.NAME);
        this.message = message;
    }
}