import BoardieEvent from "./BoardieEvent";
import GameData from "boardie/dist/GameData";

export default class SetupCompletedEvent extends BoardieEvent {
    static readonly NAME = "setupCompleted";
    readonly gameData: GameData;

    constructor(gameData: GameData) {
        super(SetupCompletedEvent.NAME);
        this.gameData = gameData;
    }
}