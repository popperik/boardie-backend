import BoardieEvent from "./BoardieEvent";
import Player from "boardie/dist/Player";

export default class PlayerStateChangedEvent extends BoardieEvent {
    static readonly NAME = "playerStateChanged";
    readonly player: Player;

    constructor(player: Player) {
        super(PlayerStateChangedEvent.NAME);
        this.player = player;
    }
}