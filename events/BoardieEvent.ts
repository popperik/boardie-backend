export default abstract class {
    readonly event: string;

    protected constructor(event: string) {
        this.event = event;
    }
}