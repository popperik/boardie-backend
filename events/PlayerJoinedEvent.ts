import BoardieEvent from "./BoardieEvent";
import Game from "boardie/dist/Game";

export default class PlayerJoinedEvent extends BoardieEvent {
    static readonly NAME = "playerJoined"
    readonly game: Game;

    constructor(game: Game) {
        super(PlayerJoinedEvent.NAME);
        this.game = game;
    }
}