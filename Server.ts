import express from "express";
import WebSocket from "ws";
import http from "http";
import * as controller from "./Controller";

export default class {
    staticRoot: string;
    port: number = 8080;
    private server = http.createServer();
    private app = express();
    private wss: WebSocket.Server;
    
    constructor(staticRoot: string) {
        this.server.on("request", this.app); // Bind Express to HTTP server
        const server = this.server;
        this.wss = new WebSocket.Server({server});
        this.staticRoot = staticRoot;
        this.registerRoutes();
    }

    start(): void {
        this.server.listen(this.port, () => console.log("boardie-backend is up and running!"));
    }

    private registerRoutes(): void {
        controller.registerRoutes(this.app, this.wss);
        this.app.use(express.static(this.staticRoot));
        this.app.get("/", (req, res) => res.sendFile("/index.html", {root: this.staticRoot}));
    }
}
